import axios from 'axios';

const API_KEY = 'e6ad7475c90aefc884482af50295f010';
const BASE_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`

export const FETCH_WEATHER = "FETCH_WEATHER";


export function fetchWeather(city) {
  const url = `${BASE_URL}&q=${city}`;
  const request = axios.get(url);
  console.log('Url: ',url);
  

  return (
    {
      type : FETCH_WEATHER,
      payload : request
    }
  )

}